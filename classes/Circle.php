<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 11.07.16
 * Time: 16:46
 */
class Circle extends Figure
{
    private $_Xpos;
    private $_Ypos;
    private $_Xrad;
    private $_Yrad;
    private $_thick;

    function __construct($col,$sx,$sy,$rx)
    {
        parent::__construct($col);
        $this->_Xpos = $sx;
        $this->_Ypos = $sy;
        $this->_Xrad = $rx;
        $this->_Yrad = $rx;
    }
    public function setThick($th){
        $this->_thick = $th;
        if ($this->_type != 'svg') {
            imagesetthickness($this->_image, $th);
        }
    }
    public function draw(){
        if ($this->_type == 'svg') {
            fwrite($this->_image, " <circle cx=\"$this->_Xpos\" cy=\"$this->_Ypos\" r=\"$this->_Xrad\"
        fill=\"none\" stroke=\"rgb($this->_r, $this->_g, $this->_b)\" stroke-width=\"$this->_thick\"  /></svg>");
        } else {
            imageellipse($this->_image, $this->_Xpos, $this->_Ypos, $this->_Xrad, $this->_Yrad, $this->_color);
        }
    }
    public function drawF(){
        if ($this->_type == 'svg') {
            fwrite($this->_image, " <circle cx=\"$this->_Xpos\" cy=\"$this->_Ypos\" r=\"$this->_Xrad\"
        fill=\"rgb($this->_r, $this->_g, $this->_b)\" stroke=\"rgb($this->_r, $this->_g, $this->_b)\" stroke-width=\"$this->_thick\"  /></svg>");
        } else {
            imagefilledellipse($this->_image, $this->_Xpos, $this->_Ypos, $this->_Xrad, $this->_Yrad, $this->_color);
        }

    }
}