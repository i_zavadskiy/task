<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 08.07.16
 * Time: 10:55
 */
class Triangle extends Figure
{
    private $_points = array();
    private $_Xpos1;
    private $_Ypos1;
    private $_Xpos2;
    private $_Ypos2;
    private $_Xpos3;
    private $_Ypos3;


    function __construct($col, $x1, $y1, $x2, $y2, $x3, $y3)
    {
        parent::__construct($col);
        $this->_points[0] = $x1;
        $this->_points[1] = $y1;
        $this->_points[2] = $x2;
        $this->_points[3] = $y2;
        $this->_points[4] = $x3;
        $this->_points[5] = $y3;
        $this->_Xpos1 = $x1;
        $this->_Ypos1 = $y1;
        $this->_Xpos2 = $x2;
        $this->_Ypos2 = $y2;
        $this->_Xpos3 = $x3;
        $this->_Ypos3 = $y3;

    }

    public function draw()
    {
        if($this->_type == 'svg') {
            fwrite($this->_image, "<polygon fill=\"rgb($this->_r, $this->_g, $this->_b)\" stroke=\"rgb($this->_r, $this->_g, $this->_b)\" stroke-width=\"1\" 
            points=\"$this->_Xpos1,$this->_Ypos1
                    $this->_Xpos2,$this->_Ypos2 $this->_Xpos3,$this->_Ypos3\" /></svg>");
        } else {
            imagefilledpolygon($this->_image, $this->_points, 3, $this->_color);
        }
    }
}