<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 07.07.16
 * Time: 17:52
 */
class Line extends Figure
{
    private $_startXpos;
    private $_startYpos;
    private $_endXpos;
    private $_endYpos;
    private $_thick = 1;

    function __construct($col, $sx, $sy, $ex, $ey)
    {
        parent::__construct($col);
        $this->_startXpos = $sx;
        $this->_startYpos = $sy;
        $this->_endXpos = $ex;
        $this->_endYpos = $ey;
    }

    public function setThick($th)
    {
        $this->_thick = $th;
        if ($this->_type != 'svg') {

            imagesetthickness($this->_image, $th);
        }
    }

    public function draw()
    {
        if ($this->_type == 'svg') {
            fwrite($this->_image, "<line x1=\"$this->_startXpos\" y1=\"$this->_startYpos\" x2=\"$this->_endXpos\" y2=\"$this->_endYpos\" stroke-width=\"$this->_thick\" stroke=\"rgb($this->_r,$this->_g,$this->_b)\"/></svg>");
       } else {
            imageline($this->_image, $this->_startXpos, $this->_startYpos, $this->_endXpos, $this->_endYpos, $this->_color);
       }
    }
}