<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 07.07.16
 * Time: 11:43
 */


class Point extends Figure
{
    private $_Xpos;
    private $_Ypos;

    function __construct($col,$x,$y)
    {
        parent::__construct($col);
        $this->_Xpos = $x;
        $this->_Ypos = $y;
    }

    public function draw(){
        if ($this->_type == 'svg') {
            fwrite($this->_image, "<circle cx=\"$this->_Xpos\" cy=\"$this->_Ypos\" r=\"1\"
        fill=\"none\" stroke=\"rgb($this->_r, $this->_g, $this->_b)\" stroke-width=\"1\"  /></svg>");
        } else {
            imagesetpixel($this->_image, $this->_Xpos, $this->_Ypos, $this->_color);
        }
    }


}