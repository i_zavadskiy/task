<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 08.07.16
 * Time: 11:17
 */
class Rectangle extends Figure
{
    private $_Xpos1;
    private $_Ypos1;
    private $_Xpos2;
    private $_Ypos2;
    private $_Xpos3;
    private $_Ypos3;
    private $_Xpos4;
    private $_Ypos4;
    private $_thick;




    function __construct($col,$x1,$y1,$x2,$y2)
    {
        parent::__construct($col);

        $this->_Xpos1 = $x1;
        $this->_Ypos1 = $y1;
        $this->_Xpos2 = $x2;
        $this->_Ypos2 = $y2;
        $this->_Xpos3 = $x2;
        $this->_Ypos3 = $y1;
        $this->_Xpos4 = $x1;
        $this->_Ypos4 = $y2;
    }

    public function setThick($th){
        $this->_thick = $th;
        if ($this->_type != 'svg') {
            imagesetthickness($this->_image, $th);
        }
    }

    public function draw(){
        if ($this->_type == 'svg') {
            fwrite($this->_image, "<polygon fill=\"none\" stroke=\"rgb($this->_r, $this->_g, $this->_b)\" stroke-width=\"$this->_thick\" 
            points=\"$this->_Xpos1,$this->_Ypos1 $this->_Xpos3,$this->_Ypos3
                    $this->_Xpos2,$this->_Ypos2 $this->_Xpos4,$this->_Ypos4\" /></svg>");
        } else {
            imagerectangle($this->_image, $this->_Xpos1, $this->_Ypos1, $this->_Xpos2, $this->_Ypos2, $this->_color);
        }
    }
}