<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 11.07.16
 * Time: 17:01
 */
class Arc extends Figure
{
    private $_Xpos;
    private $_Ypos;
    private $_Xrad;
    private $_Yrad;
    private $_Start;
    private $_End;
    private $_thick;
    function __construct($col,$sx,$sy,$rx,$ry,$s,$e)
    {
        parent::__construct($col);
        $this->_Xpos = $sx;
        $this->_Ypos = $sy;
        $this->_Xrad = $rx;
        $this->_Yrad = $ry;
        $this->_Start = $s;
        $this->_End   = $e;
    }
    public function setThick($th){
        $this->_thick = $th;
        if ($this->_type != 'svg') {
            imagesetthickness($this->_image, $th);
        }
    }
    public function draw(){
        if ($this->_type == 'svg') {
            fwrite($this->_image, " <path d=\"M$this->_Xpos,$this->_Ypos A$this->_Xrad,$this->_Yrad 0  0 , 1 $this->_Start,$this->_End\" /></svg>");
        } else {
            imagearc($this->_image, $this->_Xpos, $this->_Ypos, $this->_Xrad, $this->_Yrad, $this->_Start, $this->_End, $this->_color);
        }
    }
    public function drawF(){

        imagefilledarc($this->_image,$this->_Xpos,$this->_Ypos,$this->_Xrad,$this->_Yrad,$this->_Start,$this->_End,$this->_color,IMG_ARC_EDGED);


    }

}