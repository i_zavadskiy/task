<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 11.07.16
 * Time: 17:39
 */
class Text extends Figure
{
    private $_Xpos;
    private $_Ypos;
    private $_font;
    private $_text;


    function __construct($col,$sx,$sy,$font,$text)
    {
        parent::__construct($col);
        $this->_Xpos = $sx;
        $this->_Ypos = $sy;
        $this->_font = $font;
        $this->_text = $text;

    }

    public function draw(){
        if ($this->_type == 'svg') {
            $this->_font *=10;
            fwrite($this->_image, " <text x=\"$this->_Xpos\" y=\"$this->_Ypos\" 
        font-family=\"Verdana\" font-size=\"$this->_font\" fill=\"rgb($this->_r, $this->_g, $this->_b)\" >$this->_text</text></svg>");
        } else {
            imagestring($this->_image, $this->_font, $this->_Xpos, $this->_Ypos, $this->_text, $this->_color);
        }
    }

}