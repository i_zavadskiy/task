<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 13.07.16
 * Time: 12:06
 */
class Valid
{

    public static function check($array)
    {
        foreach($array as $key => $value) {
            if($key != 'color' && $key != 'fill' && $key != 'text' && $key != 'figure') {
                if(!preg_match('/^[0-9]+$/', $value)) {
                    return false;
                }
            }
        }
        return true;
    }
}