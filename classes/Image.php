<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 07.07.16
 * Time: 10:04
 */
abstract class Image
{
    private $_format;
    protected $_name;
    public $imgRes;


    public function __construct($type)
    {
        $this->_format = $type;

    }



    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }

    public function createImage(){
        if($this->_format == "svg"){

        }else {
            $this->imgRes = imagecreatetruecolor(500, 500);
        }

    }
    abstract public function saveImage();

}
