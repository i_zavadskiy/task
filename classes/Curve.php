<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 11.07.16
 * Time: 16:04
 */
class Curve extends Figure
{

    private $_Xpos1;
    private $_Ypos1;
    private $_Xpos2;
    private $_Ypos2;
    private $_Xpos3;
    private $_Ypos3;
    private $_Xpos4;
    private $_Ypos4;
    private $_thick;



    function __construct($col,$x1,$y1,$x2,$y2,$x3,$y3,$x4,$y4)
    {
        parent::__construct($col);
        $this->_Xpos1 = $x1;
        $this->_Ypos1 = $y1;
        $this->_Xpos2 = $x2;
        $this->_Ypos2 = $y2;
        $this->_Xpos3 = $x3;
        $this->_Ypos3 = $y3;
        $this->_Xpos4 = $x4;
        $this->_Ypos4 = $y4;
    }

    public function setThick($th){
        $this->_thick = $th;
        if ($this->_type != 'svg') {
            imagesetthickness($this->_image, $th);
        }
    }

    public function draw(){
        if ($this->_type == 'svg') {
            fwrite($this->_image, "<polyline fill=\"none\" stroke=\"rgb($this->_r,$this->_g,$this->_b)\" stroke-width=\"$this->_thick\" 
            points=\"$this->_Xpos1,$this->_Ypos1
                    $this->_Xpos2,$this->_Ypos2 $this->_Xpos3,$this->_Ypos3 $this->_Xpos4,$this->_Ypos4\" /></svg>");
        } else {
            imageline($this->_image, $this->_Xpos1, $this->_Ypos1, $this->_Xpos2, $this->_Ypos2, $this->_color);
            imageline($this->_image, $this->_Xpos2, $this->_Ypos2, $this->_Xpos3, $this->_Ypos3, $this->_color);
            imageline($this->_image, $this->_Xpos3, $this->_Ypos3, $this->_Xpos4, $this->_Ypos4, $this->_color);
        }
    }
}