<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 12.07.16
 * Time: 12:05
 */
class ImgSvg extends Image
{
    private $f;
    public function saveImage(){
        fclose($this->f);
    }


    public function newName(){
        $this->_name = "uploads/".rand(1000,9000).".svg";
    }


    public function createImage()
    {
        parent::createImage();
        $this->f = fopen("$this->_name","a+");
        fwrite($this->f,"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>
     <svg version = \"1.1\"
     baseProfile=\"full\"
     xmlns = \"http://www.w3.org/2000/svg\" 
     xmlns:xlink = \"http://www.w3.org/1999/xlink\"
     xmlns:ev = \"http://www.w3.org/2001/xml-events\"
     height = \"400px\"  width = \"400px\">
     </svg>");

    }
}