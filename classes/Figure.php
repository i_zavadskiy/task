<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 05.07.16
 * Time: 15:53
 */
abstract class Figure
{
    protected $_color = NULL;
    protected $_image = NULL;
    protected $_type = NULL;
    public $name = NULL;
    protected $_r;
    protected $_g;
    protected $_b;

    function __construct($col)
    {
        $this->_color = $col;
    }

    public function openImg($type, $name)
    {
        $this->_type = $type;
        $this->name = $name;
        switch($type) {
            case 'jpeg':
                $this->_image = imagecreatefromjpeg("$name");
                break;
            case 'png':
                $this->_image = imagecreatefrompng("$name");
                break;
            case 'svg':
                $this->_image = fopen($name, "r+");
                fseek($this->_image, -6, SEEK_END);

                break;

            //TODO
        }

    }

    public function saveImg()
    {
        switch($this->_type) {
            case 'jpeg':
                imagejpeg($this->_image, $this->name, 100);
                break;
            case 'png':
                imagepng($this->_image, $this->name);
                break;
            case 'svg':
                fclose($this->_image);
                break;

            //TODO
        }
    }

    public function createColor()
    {

        $this->_r = hexdec($this->_color[1].$this->_color[2]);
        $this->_g = hexdec($this->_color[3].$this->_color[4]);
        $this->_b = hexdec($this->_color[5].$this->_color[6]);

        if($this->_type != 'svg') {
            $this->_color = imagecolorallocate($this->_image, "$this->_r", "$this->_g", "$this->_b");
        }
    }

    abstract public function draw();
}




