<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="js/scripts.js" defer></script>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale/1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>


</head>
<body>

<div class="col-md-12 head-block">

    <h1 class="text-center col-md-6">Image creator online!</h1>


    <img src="images/4.jpg" class="center-block col-md-4" alt="lol">

</div>

<div class="col-md-6 ">
    <div class="form-group">
        <form role="form" class="form-horizontal" method="post" id="myform">
            <p><select class="form-control input-lg" id="format" size="1" name="format">

                    <option value="jpeg">jpeg</option>
                    <option value="png">png</option>
                    <option value="svg">svg</option>
                </select></p>
            <p><select class="form-control input-lg" id="figure" size="1" name="figure">
                    <option selected disabled value="none">Choose it</option>
                    <option value="point">Point</option>
                    <option value="line">Line</option>
                    <option value="curve">Curve</option>
                    <option value="triangle">Triangle</option>
                    <option value="rectangle">Rectangle</option>
                    <option value="parallelogram">Parallelogram</option>
                    <option value="square">Square</option>
                    <option value="circle">Circle</option>
                    <option value="oval">Oval</option>
                    <option value="arc">Arc</option>
                    <option value="text">Text</option>
                </select></p>


            <p id="newF">

            </p>

            <p><input class="btn btn-success center-block" id="getForecast" type="submit" value=Choose></p>
        </form>
    </div>
</div>

<div class="image col-md-6 text-center">


</div>

<footer class="navbar-fixed-bottom text-center">Author is Zavadskiy, NIX Solutions &copy</footer>
</body>
</html>