/**
 * Created by zavadskyi on 08.07.16.
 */

$('document').ready(function () {

//Hide submit button on start
    if ($('#figure').val() == null) {
        $('#getForecast').hide();
    }


//On change figure
    $('#figure').change(function (event) {

        //show submit button
        $('#getForecast').show('slow');
        //prevent default action
        event.preventDefault();

        //take data from form
        var data = $('select#figure').val();
        var imgFormat = $('select#format').serializeArray();


        //ajax query for create file
        $.ajax({
            url: "server.php",
            type: "POST",
            data: imgFormat,
            success: function (response) {
                $('.image').html('<img src="' + response + '?' + Math.random() + '" alt="альтернативный текст"> ');
            },
            error: function () {
                alert("Ошибка ");
            },
            complete: function () {
            }
        });

        //disable file type settings
        $('select#format').attr('disabled', true);

        //query for new form's elements
        console.log(imgFormat);
        switch (data) {
            case 'point':
                $('p#newF').load("forms.php div#points");
                break;
            case 'line':
                $('p#newF').load("forms.php div#lines");
                break;
            case 'triangle':
                $('p#newF').load("forms.php div#triangles");
                break;
            case 'rectangle':
                $('p#newF').load("forms.php div#rectangles");
                break;
            case 'square':
                $('p#newF').load("forms.php div#squares");
                break;
            case 'curve':
                $('p#newF').load("forms.php div#curves");
                break;
            case 'oval':
                $('p#newF').load("forms.php div#ovals");
                break;
            case 'circle':
                $('p#newF').load("forms.php div#circles");
                break;
            case 'arc':
                $('p#newF').load("forms.php div#arcs");
                break;
            case 'parallelogram':
                $('p#newF').load("forms.php div#parallelograms");
                break;
            case 'text':
                $('p#newF').load("forms.php div#texts");
                break;

            default:
                alert('Wrong');
        }

    });
//validation!
    $('#newF').on('blur', '.number', function () {
        console.log($(this).val());
        if ($(this).val() == '') {
            $(this).parent('div').parent('.form-group').removeClass('has-success').addClass('has-error');
            $('#getForecast').hide('slow');
        } else {
            $(this).parent('div').parent('.form-group').removeClass('has-error').addClass('has-success');
            $('#getForecast').show('slow');
        }

    });


    //On "submit" clikck
    $('#getForecast').click(function (e) {

        //prevent default action
        e.preventDefault();
        //take data from form
        var data1 = $('#myform').serializeArray();

        //ajax query for image
        $.ajax({
            url: "server.php",
            type: "POST",
            data: data1,
            success: function (response) {
                if (response == "error") {
                    $('.image').html('<h1>There is some input trouble here!</h1>');
                } else {
                    $('.image').html('<img src="' + response + '?' + Math.random() + '" alt="альтернативный текст"> ');
                }
            },
            error: function () {
                $('.image').html('Some trouble here :(');
            },
            complete: function () {
            }
        });

    });


});