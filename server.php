<?php
session_start();
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 08.07.16
 * Time: 15:26
 */
//header('Content-Type: application/json');
require_once 'autoload.php';
$out;
//create image
if(array_key_exists('format', $_POST)) {
    switch($_POST['format']) {

        case 'jpeg':
            $img = new ImgJpeg("jpeg");
            break;
        case 'png':
            $img = new ImgPng("png");
            break;
        case 'svg':
            $img = new ImgSvg("png");
            break;
        default:
            break;
    }
    $img->newName();
    $img->createImage();
    $img->saveImage();
    $_SESSION['name'] = $img->getName();
    $_SESSION['type'] = $_POST['format'];

}
$out = $_SESSION['name'];

//create figure on image
if(array_key_exists('figure', $_POST)) {
    if(Valid::check($_POST)) {

        switch($_POST['figure']) {

            case 'point':
                $fig = new Point($_POST['color'], $_POST['Xpos'], $_POST['Ypos']);
                break;
            case 'line':
                $fig = new Line($_POST['color'], $_POST['sXpos'], $_POST['sYpos'], $_POST['eXpos'], $_POST['eYpos']);
                break;
            case 'square':
                $fig = new Square($_POST['color'], $_POST['1Xpos'], $_POST['1Ypos'], $_POST['2Xpos'], $_POST['2Ypos']);
                break;
            case 'rectangle':
                $fig = new Rectangle($_POST['color'], $_POST['1Xpos'], $_POST['1Ypos'], $_POST['2Xpos'], $_POST['2Ypos']);
                break;
            case 'triangle':
                $fig = new Triangle($_POST['color'], $_POST['1Xpos'], $_POST['1Ypos'], $_POST['2Xpos'], $_POST['2Ypos'], $_POST['3Xpos'], $_POST['3Ypos']);
                break;
            case 'curve':
                $fig = new Curve($_POST['color'], $_POST['1Xpos'], $_POST['1Ypos'], $_POST['2Xpos'], $_POST['2Ypos'], $_POST['3Xpos'], $_POST['3Ypos'], $_POST['4Xpos'], $_POST['4Ypos']);
                break;
            case 'oval':
                $fig = new Oval($_POST['color'], $_POST['Xpos'], $_POST['Ypos'], $_POST['Xrad'], $_POST['Yrad']);
                break;
            case 'circle':
                $fig = new Circle($_POST['color'], $_POST['Xpos'], $_POST['Ypos'], $_POST['Xrad']);
                break;
            case 'arc':
                $fig = new Arc($_POST['color'], $_POST['Xpos'], $_POST['Ypos'], $_POST['Xrad'], $_POST['Yrad'], $_POST['start'], $_POST['end']);
                break;
            case 'parallelogram':
                $fig = new Parallelogram($_POST['color'], $_POST['1Xpos'], $_POST['1Ypos'], $_POST['2Xpos'], $_POST['2Ypos'], $_POST['3Xpos'], $_POST['3Ypos'], $_POST['4Xpos'], $_POST['4Ypos']);
                break;
            case 'text':
                $fig = new Text($_POST['color'], $_POST['Xpos'], $_POST['Ypos'], $_POST['font'], $_POST['text']);

            default:
                $out = "error";
                break;
        }

        $fig->openImg($_SESSION['type'], $_SESSION['name']);
        $fig->createColor();
        if(array_key_exists('thick', $_POST)) {
            $fig->setThick($_POST['thick']);
        }
        if(array_key_exists('fill', $_POST)) {
            $fig->drawF();
        } else {
            $fig->draw();
        }
        $fig->saveImg();
        $out = $_SESSION['name'];
    } else {
        $out = "error";
    }
}
echo $out;

