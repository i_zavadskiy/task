<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>


<div id="points">
    <div class="form-group">
        <label for="point_x" class="col-sm-2 control-label">X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Xpos" id="point_x">
        </div>
    </div>
    <div class="form-group">
        <label for="point_y" class="col-sm-2 control-label">Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg  number" type="number" name="Ypos" id="point_y">
        </div>
    </div>
    <div class="form-group">
        <label for="point_col" class="col-sm-2 control-label">Color</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="color" name="color"id="point_col">
        </div>
    </div>
</div>


<!--line-->

<div id="lines">
    <div class="form-group">
        <label for="line_1x" class="col-sm-2 control-label">Start X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="sXpos" id="line_1x">
        </div>
    </div>
    <div class="form-group">
        <label for="line_1y" class="col-sm-2 control-label">Start Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="sYpos" id="line_1y">
        </div>
    </div>
    <div class="form-group">
        <label for="line_2x" class="col-sm-2 control-label">End X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="eXpos" id="line_2x">
        </div>
    </div>
    <div class="form-group">
        <label for="line_2y" class="col-sm-2 control-label">End Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="eYpos" id="line_2y">
        </div>
    </div>

    <div class="form-group">
        <label for="line_th" class="col-sm-2 control-label">thickness</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="thick" id="line_th">
        </div>
    </div>

    <div class="form-group">
        <label for="line_col" class="col-sm-2 control-label">Color</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="color" name="color"id="line_col">
        </div>
    </div>
</div>


<!--curve-->
<div id="curves">
    <div class="form-group">
        <label for="curve_1x" class="col-sm-2 control-label">1 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="1Xpos" id="curve_1x">
        </div>
    </div>
    <div class="form-group">
        <label for="curve_1y" class="col-sm-2 control-label">1 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="1Ypos" id="curve_1y">
        </div>
    </div>
    <div class="form-group">
        <label for="curve_2x" class="col-sm-2 control-label">2 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="2Xpos" id="curve_2x">
        </div>
    </div>
    <div class="form-group">
        <label for="curve_2y" class="col-sm-2 control-label">2 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="2Ypos" id="curve_2y">
        </div>
    </div>
    <div class="form-group">
        <label for="curve_2x" class="col-sm-2 control-label">3 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="3Xpos" id="curve_2x">
        </div>
    </div>
    <div class="form-group">
        <label for="curve_2y" class="col-sm-2 control-label">3 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="3Ypos" id="curve_2y">
        </div>
    </div>
    <div class="form-group">
        <label for="curve_2x" class="col-sm-2 control-label">4 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="4Xpos" id="curve_2x">
        </div>
    </div>
    <div class="form-group">
        <label for="curve_2y" class="col-sm-2 control-label">4 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="4Ypos" id="curve_2y">
        </div>
    </div>

    <div class="form-group">
        <label for="curve_th" class="col-sm-2 control-label">thickness</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="thick" id="curve_th">
        </div>
    </div>

    <div class="form-group">
        <label for="curve_col" class="col-sm-2 control-label">Color</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="color" name="color"id="curve_col">
        </div>
    </div>
</div>

<!--triangle-->
<div id="triangles">
    <div class="form-group">
        <label for="triangle_1x" class="col-sm-2 control-label">1 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="1Xpos" id="triangle_1x">
        </div>
    </div>
    <div class="form-group">
        <label for="triangle_1y" class="col-sm-2 control-label">1 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="1Ypos" id="triangle_1y">
        </div>
    </div>
    <div class="form-group">
        <label for="triangle_2x" class="col-sm-2 control-label">2 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="2Xpos" id="triangle_2x">
        </div>
    </div>
    <div class="form-group">
        <label for="triangle_2y" class="col-sm-2 control-label">2 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="2Ypos" id="triangle_2y">
        </div>
    </div>
    <div class="form-group">
        <label for="triangle_3x" class="col-sm-2 control-label">3 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="3Xpos" id="triangle_3x">
        </div>
    </div>
    <div class="form-group">
        <label for="triangle_3y" class="col-sm-2 control-label">3 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="3Ypos" id="triangle_3y">
        </div>
    </div>
    <div class="form-group">
        <label for="triangle_col" class="col-sm-2 control-label">Color</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="color" name="color"id="triangle_col">
        </div>
    </div>
</div>


<!--rectangle-->
<div id="rectangles">
    <div class="form-group">
        <label for="rectangle_1x" class="col-sm-2 control-label">1 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="1Xpos" id="rectangle_1x">
        </div>
    </div>
    <div class="form-group">
        <label for="rectangle_1y" class="col-sm-2 control-label">1 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="1Ypos" id="rectangle_1y">
        </div>
    </div>
    <div class="form-group">
        <label for="rectangle_2x" class="col-sm-2 control-label">2 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="2Xpos" id="rectangle_2x">
        </div>
    </div>
    <div class="form-group">
        <label for="rectangle_2y" class="col-sm-2 control-label">2 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="2Ypos" id="rectangle_2y">
        </div>
    </div>

    <div class="form-group">
        <label for="rectangle_th" class="col-sm-2 control-label">thickness</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="thick" id="rectangle_th">
        </div>
    </div>


    <div class="form-group">
        <label for="rectangle_col" class="col-sm-2 control-label">Color</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="color" name="color"id="rectangle_col">
        </div>
    </div>
</div>


<!--parallelogram-->
<div id="parallelograms">
    <div class="form-group">
        <label for="parallelogram_1x" class="col-sm-2 control-label">1 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="1Xpos" id="parallelogram_1x">
        </div>
    </div>
    <div class="form-group">
        <label for="parallelogram_1y" class="col-sm-2 control-label">1 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="1Ypos" id="parallelogram_1y">
        </div>
    </div>
    <div class="form-group">
        <label for="parallelogram_2x" class="col-sm-2 control-label">2 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="2Xpos" id="parallelogram_2x">
        </div>
    </div>
    <div class="form-group">
        <label for="parallelogram_2y" class="col-sm-2 control-label">2 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="2Ypos" id="parallelogram_2y">
        </div>
    </div>
    <div class="form-group">
        <label for="parallelogram_3x" class="col-sm-2 control-label">3 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="3Xpos" id="parallelogram_3x">
        </div>
    </div>
    <div class="form-group">
        <label for="parallelogram_3y" class="col-sm-2 control-label">3 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="3Ypos" id="parallelogram_3y">
        </div>
    </div>
    <div class="form-group">
        <label for="parallelogram_4x" class="col-sm-2 control-label">3 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="4Xpos" id="parallelogram_4x">
        </div>
    </div>
    <div class="form-group">
        <label for="parallelogram_4y" class="col-sm-2 control-label">4 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="4Ypos" id="parallelogram_4y">
        </div>
    </div>
    <div class="form-group">
        <label for="parallelogram_col" class="col-sm-2 control-label">Color</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="color" name="color"id="parallelogram_col">
        </div>
    </div>
</div>

<!--square-->
<div id="squares">


    <div class="form-group">
        <label for="square_1x" class="col-sm-2 control-label">1 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="1Xpos" id="square_1x">
        </div>
    </div>
    <div class="form-group">
        <label for="square_1y" class="col-sm-2 control-label">1 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="1Ypos" id="square_1y">
        </div>
    </div>
    <div class="form-group">
        <label for="square_2x" class="col-sm-2 control-label">2 X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="2Xpos" id="square_2x">
        </div>
    </div>
    <div class="form-group">
        <label for="square_2y" class="col-sm-2 control-label">2 Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="2Ypos" id="square_2y">
        </div>
    </div>

    <div class="form-group">
        <label for="square_col" class="col-sm-2 control-label">Color</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="color" name="color"id="square_col">
        </div>
    </div>
</div>

<!--circle-->
<div id="circles">
    <div class="form-group">
        <label for="circle_x" class="col-sm-2 control-label">X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Xpos" id="circle_x">
        </div>
    </div>
    <div class="form-group">
        <label for="circle_y" class="col-sm-2 control-label">Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Ypos" id="circle_y">
        </div>
    </div>
    <div class="form-group">
        <label for="circle_rad" class="col-sm-2 control-label">X radius</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Xrad" id="circle_rad">
        </div>
    </div>

<!--    <div class="form-group">-->
<!--        <label for="circle_th" class="col-sm-2 control-label">thickness</label>-->
<!--        <div class="col-sm-10">-->
<!--            <input class="form-control input-lg number" type="number" name="thick" id="circle_th">-->
<!--        </div>-->
<!--    </div>-->

    <!--fill-->
    <div class="form-group">
        <label for="circle_fill" class="col-sm-2 control-label">fill</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="checkbox" name="fill" id="circle_fill">
        </div>
    </div>


    <div class="form-group">
        <label for="circle_col" class="col-sm-2 control-label">Color</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="color" name="color"id="circle_col">
        </div>
    </div>
</div>

<!--oval-->
<div id="ovals">
    <div class="form-group">
        <label for="oval_x" class="col-sm-2 control-label">X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Xpos" id="oval_x">
        </div>
    </div>
    <div class="form-group">
        <label for="oval_y" class="col-sm-2 control-label">Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Ypos" id="oval_y">
        </div>
    </div>
    <div class="form-group">
        <label for="oval_x_rad" class="col-sm-2 control-label">X radius</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Xrad" id="oval_x_rad">
        </div>
    </div>
    <div class="form-group">
        <label for="oval_y_rad" class="col-sm-2 control-label">Y radius</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Yrad" id="oval_y_rad">
        </div>
    </div>

<!--    <div class="form-group">-->
<!--        <label for="oval_th" class="col-sm-2 control-label">thickness</label>-->
<!--        <div class="col-sm-10">-->
<!--            <input class="form-control input-lg number" type="number" name="thick" id="oval_th">-->
<!--        </div>-->
<!--    </div>-->

<!--fill-->
    <div class="form-group">
        <label for="oval_fill" class="col-sm-2 control-label">fill</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="checkbox" name="fill" id="oval_fill">
        </div>
    </div>



    <div class="form-group">
        <label for="oval_col" class="col-sm-2 control-label">Color</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="color" name="color"id="oval_col">
        </div>
    </div>
</div>

<!--arc-->
<div id="arcs">
    <div class="form-group">
        <label for="arc_x" class="col-sm-2 control-label">X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Xpos" id="arc_x">
        </div>
    </div>
    <div class="form-group">
        <label for="arc_y" class="col-sm-2 control-label">Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Ypos" id="arc_y">
        </div>
    </div>
    <div class="form-group">
        <label for="arc_x_rad" class="col-sm-2 control-label">X radius</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Xrad" id="arc_x_rad">
        </div>
    </div>
    <div class="form-group">
        <label for="arc_y_rad" class="col-sm-2 control-label">Y radius</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Yrad" id="arc_y_rad">
        </div>
    </div>

    <div class="form-group">
        <label for="arc_s" class="col-sm-2 control-label">Start</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="start" id="arc_s">
        </div>
    </div>
    <div class="form-group">
        <label for="arc_e" class="col-sm-2 control-label">End</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="end" id="arc_e">
        </div>
    </div>


    <div class="form-group">
        <label for="arc_th" class="col-sm-2 control-label">thickness</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="thick" id="arc_th">
        </div>
    </div>

    <!--fill-->
    <div class="form-group">
        <label for="arc_fill" class="col-sm-2 control-label">fill</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="checkbox" name="fill" id="arc_fill">
        </div>
    </div>



    <div class="form-group">
        <label for="oval_col" class="col-sm-2 control-label">Color</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="color" name="color"id="oval_col">
        </div>
    </div>
</div>




<!--text-->

<div id="texts">
    <div class="form-group">
        <label for="text_x" class="col-sm-2 control-label">X pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Xpos" id="text_x">
        </div>
    </div>
    <div class="form-group">
        <label for="text_y" class="col-sm-2 control-label">Y pos</label>
        <div class="col-sm-10">
            <input class="form-control input-lg number" type="number" name="Ypos" id="text_y">
        </div>
    </div>
    <div class="form-group">
        <label for="size" class="col-sm-2 control-label">Size</label>
        <div class="col-sm-10">
        <select class="form-control input-lg" id="size" size="1" name="font">

            <option value="1">Size 1</option>
            <option value="2">Size 2</option>
            <option value="2">Size 3</option>
            <option value="4">Size 4</option>
            <option value="5">Size 5</option>
        </select>
        </div>
    </div>
    <div class="form-group">
        <label for="text" class="col-sm-2 control-label">Text</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="text" name="text" id="text">
        </div>
    </div>


    <div class="form-group">
        <label for="text_col" class="col-sm-2 control-label">Color</label>
        <div class="col-sm-10">
            <input class="form-control input-lg" type="color" name="color"id="text_col">
        </div>
    </div>
</div>


</body>
</html>